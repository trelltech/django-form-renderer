# Opinionated form rendering

This Django application provides two template tags that can be used to render
reasonably flexible forms.

## Installation

1. Install directly from this repo (while unstable) with `pip install
   -e git+https://bitbucket.org/trelltech/django-form-renderer.git#egg=form_renderer`
2. Add to `INSTALLED_APPS`
3. Load the tag library in the templates in which you wish to use the included
   tags: `{% load form_renderer %}`


## Template tags

### Render form (`render_form`)

Render an entire form including hidden fields and non-field errors.

The tag takes four arguments:

- `form` the form on which to operate. 
- `placeholder_fields` is a comma-separated list of fields that should have
  `placeholders` (see `render_field` below).
- `exclude_fields` is a comma-separated list of fields that should be
  excluded.
- `grouped_fields` is a simple way of grouping several fields. Fields that
  should be grouped are separated by a plus sign. Several groups are separated
  by commas.

The generated HTML will contain various elements and classes that enable
fine-grained control over how the form is displayed.

Example:

    {% render_form form
       placeholder_fields="email,first_name,last_name,password1,password2"
       grouped_fields="first_name+last_name,password1+password2"
       exclude_fields="username"
    %}


### Render field (`render_field`)

Render a single field.

The tag takes two arguments:

- `field` the field in question.
- `inject_placeholder` dictates whether or not to add a placeholder attribute
  to the form. The value of the attribute is the same as the label of the
  form. The element wrapping the field will receive a class name that dictates
  whether the field has a placeholder value or not.

Example:

    {% render_field email inject_placeholder=True %}

## Templates

There are two templates used by the tags in this application:
`form_renderer/form.html` and `form_renderer/field.html`. Both templates are
quite straight-forward and can be overridden in order to tweak the output.

## TODO

Well, hopefully not much. I'm considering including `help_text` in the output,
but it is slightly harder to anticipate where the developer would want to
place it (after label or after field?). A setting could be used to dictate the
location, but I want to keep arguments down. I'm also considering a few simple
settings that would allow the developer to control the output of the
placeholder attribute.
