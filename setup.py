#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='django-form-renderer', version='0.1',
      description='Opinionated form rendering for Django.',
      author='Gustaf Sjoberg', author_email='gs@trell.se',
      include_package_data=True, packages=find_packages())
