import collections
import string

from django import template
from django.forms.fields import CheckboxInput
from django import forms


register = template.Library()


@register.inclusion_tag('form_renderer/form.html')
def render_form(form, placeholder_fields='', exclude_fields='',
                grouped_fields=''):
    exclude_fields = map(string.strip, exclude_fields.split(','))
    placeholder_fields = map(string.strip, placeholder_fields.split(','))

    added_fields = []

    grouped_field_parents = {}
    for field_group in grouped_fields.split(','):
        field_names = map(string.strip, field_group.split('+'))
        for (position, field_name) in enumerate(field_names):
            grouped_field_parents[field_name] = (position, field_names[0])

    field_groups = collections.OrderedDict()
    for field in form.visible_fields():
        if field.name in exclude_fields:
            continue

        field.inject_placeholder = field.name in placeholder_fields

        if grouped_field_parents.has_key(field.name):
            (position, parent_field_name) = grouped_field_parents[field.name]
        else:
            (position, parent_field_name) = (0, field.name)

        if not field_groups.has_key(parent_field_name):
            field_groups[parent_field_name] = []

        field_groups[parent_field_name].insert(position, field)

    return {
        'form': form,
        'field_groups': field_groups,
    }


@register.inclusion_tag('form_renderer/field.html')
def render_field(field, inject_placeholder=False):
    if inject_placeholder:
        field = _inject_placeholder(field)

    return {
        'field': field,
        'is_checkbox': isinstance(field.field.widget, CheckboxInput),
        'inject_placeholder': inject_placeholder,
    }


def _inject_placeholder(field):
    """Inject a placeholder attribute into fieldtypes where it makes sense."""
    placeholder_fields = (
        forms.widgets.TextInput,
        forms.widgets.EmailInput,
        forms.widgets.PasswordInput,
    )
    
    if isinstance(field.field.widget, placeholder_fields):
        field.field.widget.attrs['placeholder'] = field.label

    return field
